# watter_alert

This script is tested in Ubuntu 16.04

This script when executed and activated will alert you to drink some watter each 15 minutes.

This application works only if you place it in the /opt directory.

You can create a desktop icon for you to be more confortable to load the app.

You have to create a new file /usr/share/applications/watter-alert.desktop with the following content:

```php
[Desktop Entry]
Version=1.0
Type=Application
Name=WatterAlert
Icon=/opt/watter_alert/gota_agua.png
Exec="/opt/watter_alert/watter_alert.py" %f
Comment=Watter alert for people
Categories=Development;IDE;
Terminal=false
StartupWMClass=watter-alert
```
