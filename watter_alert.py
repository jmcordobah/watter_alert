#!/usr/bin/env python

import os
import signal
import json
import gi
import time
import datetime

from urllib2 import Request, urlopen, URLError

from gi.repository import GLib

gi.require_version('AppIndicator3', '0.1')
from gi.repository import AppIndicator3 as appindicator
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify as notify

from gi.repository import Notify, GdkPixbuf

APPINDICATOR_ID = 'watter_alert'

def main():
    indicator = appindicator.Indicator.new(APPINDICATOR_ID, os.path.abspath('/opt/watter_alert/gota_agua.png'), appindicator.IndicatorCategory.SYSTEM_SERVICES)
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
    indicator.set_menu(build_menu())
    notify.init(APPINDICATOR_ID)
    gtk.main()

def build_menu():
    menu = gtk.Menu()

    item_15 = gtk.MenuItem('Alert every 15 minutes')
    item_15.connect('activate', loop1)
    menu.append(item_15)

    item_quit = gtk.MenuItem('Quit')
    item_quit.connect('activate', quit)
    menu.append(item_quit)

    menu.show_all()

    return menu

def mensaje_15min():

    msg = '15 minutes over...it is time to drink watter!'
    return msg

def loop1(_):

    while True:

        message = "15 minutes over,\ndrink some watter dude..."

        now = datetime.datetime.now()
        if now.minute%15==0:
            n = notify.Notification.new("<b>Drink watter!</b>\n", message, None)
            image = GdkPixbuf.Pixbuf.new_from_file("/opt/watter_alert/gota_agua.png")
            n.set_icon_from_pixbuf(image)
            n.set_image_from_pixbuf(image)
            n.show()

        time.sleep(30)

def quit(_):

    notify.uninit()
    gtk.main_quit()

if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
